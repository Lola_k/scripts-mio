# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 14:27:37 2020

@author: Lola
"""

#from netCDF4 import Dataset
import xarray as xr #Lib/site-packages
import numpy as np
import gsw
import matplotlib.pyplot as plt
#import netCDF4

transchoice = [1, 2, 3, 4, 5, 6, 7]

#sio.netcdf_file("PL2.nc")
#sio.netcdf_variable

for p in transchoice:
#    trans = str(transchoice[p])
    PL = xr.open_dataset("PL" + str(p) +".nc")
#PL1 = xr.open_dataset('PL1.nc')


    temp = PL['temp']
    sal = PL['sal']
    pres = PL['pres']
    lon = PL['lon']
    lat = PL['lat']


    # Figure out boudaries (mins and maxs)
    smin = np.nanmin(sal) - (0.01 * np.nanmin(sal))
    smax = np.nanmax(sal) + (0.01 * np.nanmax(sal))
    tmin = np.nanmin(temp) - (0.1 * np.nanmin(temp))
    tmax = np.nanmax(temp) + (0.1 * np.nanmax(temp))
     
    # Calculate how many gridcells we need in the x and y dimensions
    xdim = round((smax-smin)*10+1,0)
    ydim = round((tmax-tmin)+1,0)
          
    # Create temp and salt vectors of appropiate dimensions
    ti = np.linspace(1,ydim-1,ydim)+tmin
    si = np.linspace(1,xdim-1,xdim)/10+smin

    # Create empty grid of zeros    
    dens = np.zeros(((len(ti)),len(si)))
     
    # Loop to fill in grid with densities
    for j in range(0,int(ydim)):
        for i in range(0, int(xdim)):
            dens[j,i]=gsw.rho(si[i],ti[j],0)  
        
    # Substract 1000 to convert to sigma-t
    dens = dens-1000
    
    
    ### PL1 ###
    
#    lon1 = np.where(lat>=7.5)
#    lon2 = np.where(lon<7.5)
#    pres1 = np.where(pres>100)
    
#    eau1 = np.where(lat<=43.4)
#    eau2 = np.where((lat>=43) & (lat<=43.32))
#    pres1 = np.where(pres>=100)
    
    if p == 1:
        eau1 = np.where((lat>=42) & (lat<=43.32)) #r
        eau2 = np.where((lat>=43.61) & (lat<=44.9)) #y
        eau3 = np.where((lat>=43.32) & (lat<43.61)) #c
        
    if p == 2:
        eau1 = np.where((lon>=8) & (lon<=8.68)) #c
        eau2 = np.where((lon>=8.68) & (lon<=8.89)) #y
        eau3 = np.where((lon>=8.89) & (lon<=9.5)) #r
        #pres1 = np.where(pres<=4)
        
    if p == 3:
        eau1 = np.where((lat>=43.1) & (lat<43.5)) #c
        eau2 = np.where((lat>=43.5) & (lat<=44.9)) #y
        eau3 = np.where((lat>=42) & (lat<=43.1)) #r
#        pres1 = np.where(pres>=100)
        
    if p == 4:
        eau1 = np.where((lat>=42.8) & (lat<43.52)) #c
        eau2 = np.where((lat>=43.8) & (lat<=44.9)) #y
        eau3 = np.where((lat>=43.52) & (lat<=43.8)) #r
        #pres1 = np.where(pres>=80)
    
    if p == 5:
        eau1 = np.where((lat>=43) & (lat<43.75)) #c
        eau2 = np.where((lat>=43.75) & (lat<=44.9)) #y
        eau3 = np.where((lat>=43.365) & (lat<=43.45)) #r
        #pres1 = np.where(pres>=100)
    
    if p == 6:
        eau1 = np.where((lat>=43.4) & (lat<43.61)) #c
        eau2 = np.where((lat>=43.61) & (lat<=43.9)) #y
        eau3 = np.where((lat>=43.705) & (lat<=43.707)) #r
        #pres1 = np.where(pres>=100)
        
    if p == 7:
        eau1 = np.where((lat>=43.4) & (lat<43.7) & (lon>=8)) #c
        eau2 = np.where((lat>=43.6) & (lat<=43.9) & (lon<=8.1)) #y
        eau3 = np.where(lon>=8.1) #r
#        pres1 = np.where(pres<=8)
        
     
    
    # Plot data ***********************************************
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    CS = plt.contour(si,ti,dens,10, linestyles='dashed', colors='k')

    plt.clabel(CS, fontsize=10, inline=1) # Label every second level
    plt.title('PL' + str(p) +'\n Diagramme T-S')
     
    ax1.plot(sal,temp,'ok', markersize=1)
     
    ax1.set_xlabel('Salinity')
    ax1.set_ylabel('Temperature (C)')
    
    plt.xlim(37.7,38.9)
    plt.ylim(13,16.5)
    
    #if ((lon>8)==True):
    #    lon1 = np.where(lon>8)
    #    plt.scatter(sal[lon1[0]],temp[lon1[0]],c = 'b')
    #
    #if ((lon<8)==True):
    #    lon2 = np.where(lon<8)
    #    plt.scatter(sal[lon2[0]], temp[lon2[0]], c = 'y')
    ### Color masses d'eau ###
    
    plt.plot(sal[:,eau2[0]], temp[:,eau2[0]],'oy', markersize = 1)
    plt.plot(sal[:,eau1[0]],temp[:,eau1[0]],'oc', markersize = 1)

    plt.plot(sal[:,eau3[0]], temp[:,eau3[0]],'or', markersize = 1)

#    plt.plot(sal[pres1[0]],temp[pres1[0]], 'ok', markersize = 0.5)
    
    plt.show()