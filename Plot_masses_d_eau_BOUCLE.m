pkg load netcdf;

transchoice = [1 2 3 4 5 6 7];

##for i = transchoice ;
  
##### LOAD TRANSECT ###
##PL = load(["PL", num2str(i), ".nc"]);
PL = load('PL6.nc');

lat = PL.("lat");
lon = PL.('lon');
temp = PL.("temp");
sal = PL.('sal');
dist = PL.('dist');
pres = PL.('pres');
dens = PL.('dens');
fluo = PL.('fluo');

LIW = find(sal>=38.5 & sal<=38.7);
b = find(sal>=38.7);

### TEST MASSES D'EAUX ###
eau1 = find(sal>38.2 & temp>=15.06); # blue up
eau2 = find(sal>38.26 & sal<=38.40 & temp<=15 & temp>=13.88); #lien BLEU et red
eau3 = find(sal>38.32 & sal<38.5 & temp>=13 & temp<=13.87); #coude GREEN
eau4 = find(sal>38.08 & sal<=38.2 & temp>=15 & temp<=16); # red up
eau5 = find(sal>38.11 & sal<=38.32 & temp>=13.57 & temp<=13.84); # YELLOW1
eau6 = find(sal>38.03 & sal<=38.16 & temp>=13.84 & temp<=14.5); # YELLOW1 (VERT FONCE)

eau7 = find(sal>37.5 & sal<=37.91 & temp>=15.31 & temp<=17); ##MAGENTA
eau8 = find(sal>37.91 & sal<=38.08 & temp>=15 & temp<=17); ##MAGENTA
eau9 = find(sal>37.97 & sal<=38.16 & temp>=14.5 & temp<=15); ##JAUNE

##eau10 = find(sal>38.11 & sal<=38.25 & temp>=14.5 & temp<=15); ##JAUNE

eau11 = find(sal>37.78 & sal<=37.88 & temp>=14.5 & temp<=15.32); ##VERT FONCE
eau12 = find(sal>38.17 & sal<=38.26 & temp>=13.84 & temp<=14.8); ##ROUGE
eau13 = find(sal>38.12 & sal<=38.2 & temp>=14.35 & temp<=14.81); ##ROUGE

# PLOT ###
##figure;
##
##plot(dist(LIW), -pres(LIW), '.k', 'markersize', 10); # LIW BLACK
##hold on; plot(dist(b), -pres(b), '.k', 'markersize', 10); # SAL>38.7 MAGENTA
##hold on; plot(dist(eau1), -pres(eau1), '.c', 'markersize', 10);
##hold on; plot(dist(eau2), -pres(eau2), '.b', 'markersize', 10);
##hold on; plot(dist(eau3), -pres(eau3), '.g', 'markersize', 10);
##hold on; plot(dist(eau4), -pres(eau4), '.r', 'markersize', 10);
##hold on; plot(dist(eau5), -pres(eau5), '.', 'color', [1,0.8,0], 'markersize', 10);
##hold on; plot(dist(eau6), -pres(eau6), '.', 'color', [1,0.8,0], 'markersize', 10);
####hold on; plot(dist(eau6), -pres(eau6), '.', 'color', [0,0.5,0], 'markersize', 10);
##hold on; plot(dist(eau7), -pres(eau7), '*m', 'markersize', 8);
##hold on; plot(dist(eau8), -pres(eau8), '*m', 'markersize', 8);
##hold on; plot(dist(eau9), -pres(eau9), '*y', 'markersize', 7);
####hold on; plot(dist(eau10), -pres(eau10), '*y', 'markersize', 7);
##hold on; plot(dist(eau11), -pres(eau11), '*', 'color', [0,0.5,0], 'markersize', 7);
##hold on; plot(dist(eau12), -pres(eau12), '*', 'color', [0.8,0,0], 'markersize', 5);
##hold on; plot(dist(eau13), -pres(eau13), '*', 'color', [0.8,0,0], 'markersize', 5);
##
##xlabel('Distance (km)');
##ylabel('Pressure (dbar)');
##title(['PL',num2str(i)]);
##ylim([-50,-5]);
##figure;
##pcolor(dist, -pres, fluo);
##shading interp;
##xlabel('Distance (km)');
##ylabel('Fluorescence (mg/L)');
##title(['PL',num2str(i)]);

##figure;
plot(sal(LIW), temp(LIW), '.k', 'markersize', 10); # LIW BLACK
hold on; plot(sal(b), temp(b), '.k', 'markersize', 10); # SAL>38.7 MAGENTA

hold on; plot(sal(eau1), temp(eau1), '.c', 'markersize', 10);
hold on; plot(sal(eau2), temp(eau2), '.b', 'markersize', 10);

hold on; plot(sal(eau3), temp(eau3), '.g', 'markersize', 10);

hold on; plot(sal(eau4), temp(eau4), '.r', 'markersize', 10);

hold on; plot(sal(eau5), temp(eau5), '.', 'color', [1,0.8,0], 'markersize', 10);
hold on; plot(sal(eau6), temp(eau6), '.', 'color', [1,0.8,0], 'markersize', 10);
##hold on; plot(sal(eau6), temp(eau6), '.', 'color', [0,0.5,0], 'markersize', 10);
hold on; plot(sal(eau7), temp(eau7), '*m', 'markersize', 8);
hold on; plot(sal(eau8), temp(eau8), '*m', 'markersize', 8);
hold on; plot(sal(eau9), temp(eau9), '*y', 'markersize', 7);
##hold on; plot(sal(eau10), temp(eau10), '*y', 'markersize', 7);
hold on; plot(sal(eau11), temp(eau11), '*', 'color', [0,0.5,0], 'markersize', 7);
hold on; plot(sal(eau12), temp(eau12), '*', 'color', [0.8,0,0], 'markersize', 5);
hold on; plot(sal(eau13), temp(eau13), '*', 'color', [0.8,0,0], 'markersize', 5);

xlabel('Salinit�');
ylabel('Temp�rature (�C)');
title(['PL',num2str(i)]);
xlim([37.7,38.9]);
ylim([13,16.5]);
grid on



##end  