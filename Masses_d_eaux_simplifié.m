pkg load netcdf;

transchoice = [1 2 3 4 5 6 7];

##f = figure;

for i = transchoice ;
  
##### LOAD TRANSECT ###
PL = load(["PL", num2str(i), ".nc"]);
##PL = load('PL6.nc');

lat = PL.("lat");
lon = PL.('lon');
temp = PL.("temp");
sal = PL.('sal');
dist = PL.('dist');
pres = PL.('pres');
dens = PL.('dens');
fluo = PL.('fluo');

LIW = find(sal>=38.43 & sal<=38.7);
b = find(sal>=38.7);


#####################################################################################
### TEST MASSES D'EAUX ###
eau1 = find(sal>38.2 & temp>=15.0); # blue up
eau2 = find(sal>38.26 & sal<=38.43 & temp<=15.1 & temp>=13.88); #lien BLEU et red
eau21 = find(sal>38.22 & sal<=38.3 & temp<=15.1 & temp>=14.6); #lien BLEU et red

eau3 = find(sal>38.28 & sal<38.43 & temp>=13 & temp<=13.87); #coude GREEN DEVENU BLACK

eau4 = find(sal>38.08 & sal<=38.2 & temp>=15 & temp<=16); # red up

eau5 = find(sal>38.11 & sal<=38.28 & temp>=13.57 & temp<=13.84); # ORANGE
eau6 = find(sal>37.97 & sal<=38.16 & temp>=13.84 & temp<=14.5); # ORANGE (VERT FONCE)
eau56 = find(sal>38.03 & sal<=38.18 & temp>=13.84 & temp<=14); # ORANGE (VERT FONCE)
eau565 = find(sal>37.93 & sal<=38 & temp>=14.72 & temp<=15.1); # ORANGE (VERT FONCE)

eau7 = find(sal>37.5 & sal<=37.91 & temp>=15.31 & temp<=17); ##MAGENTA
eau8 = find(sal>37.91 & sal<=38.08 & temp>=15 & temp<=17); ##MAGENTA

eau9 = find(sal>37.97 & sal<=38.12 & temp>=14.45 & temp<=15); ##JAUNE

eau11 = find(sal>37.78 & sal<=37.88 & temp>=14.5 & temp<=15.32); ##VERT FONCE
eau111 = find(sal>37.8 & sal<=37.96 & temp>=13.84 & temp<=14.77); ##VERT FONCE
eau111111 = find(sal>37.8 & sal<=37.93 & temp>=13.84 & temp<=15); ##VERT FONCE
%eau1111 = find(sal>37.97 & sal<=38 & temp>=14 & temp<=14.4); ##VERT FONCE
%eau11111 = find(sal>37.97 & sal<=38.0 & temp>=13.9 & temp<=14.3); ##VERT FONCE

eau12 = find(sal>38.17 & sal<=38.26 & temp>=14 & temp<=14.6); ##BORDEAUX
eau13 = find(sal>38.12 & sal<=38.22 & temp>=14.35 & temp<=15); ##BORDEAUX
eau123 = find(sal>38.18 & sal<=38.28 & temp>=13.84 & temp<=14.1); ##BORDEAUX
#####################################################################################


## PLOT ####

#########################################################################
figure;
plot(sal(LIW), temp(LIW), '.k', 'markersize', 10); # LIW BLACK
hold on; plot(sal(b), temp(b), '.k', 'markersize', 10); # SAL>38.7 MAGENTA

### BLEU ###
hold on; plot(sal(eau1), temp(eau1), '.b', 'markersize', 10); %6*
hold on; plot(sal(eau2), temp(eau2), '.b', 'markersize', 10);
hold on; plot(sal(eau21), temp(eau21), '.b', 'markersize', 10);

hold on; plot(sal(eau3), temp(eau3), '.g', 'markersize', 10);

hold on; plot(sal(eau4), temp(eau4), '.', 'color', [0.8,0,0], 'markersize', 10); %*

### ORANGE ###
hold on; plot(sal(eau5), temp(eau5), '.', 'color', [1,0.8,0], 'markersize', 10);
hold on; plot(sal(eau6), temp(eau6), '.', 'color', [1,0.8,0], 'markersize', 10);
hold on; plot(sal(eau56), temp(eau56), '.', 'color', [1,0.8,0], 'markersize', 10);
hold on; plot(sal(eau565), temp(eau565), '.', 'color', [1,0.8,0], 'markersize', 10);

hold on; plot(sal(eau7), temp(eau7), '.','color', [1,0.8,0], 'markersize', 10); %*
hold on; plot(sal(eau8), temp(eau8), '.','color', [1,0.8,0], 'markersize', 10); %*
hold on; plot(sal(eau9), temp(eau9), '.','color', [1,0.8,0], 'markersize', 10);


### VERT FONCE ###
hold on; plot(sal(eau11), temp(eau11), '.', 'color', [0,0.5,0], 'markersize', 10); %*
hold on; plot(sal(eau111), temp(eau111), '.', 'color', [0,0.5,0], 'markersize', 10); %*
hold on; plot(sal(eau111111), temp(eau111111), '.', 'color', [0,0.5,0], 'markersize', 10); %*
%hold on; plot(sal(eau1111), temp(eau1111), '*', 'color', [0,0.5,0], 'markersize', 6);
%hold on; plot(sal(eau11111), temp(eau11111), '*', 'color', [0,0.5,0], 'markersize', 6);

### BORDEAUX ###
hold on; plot(sal(eau12), temp(eau12), '.', 'color', [0.8,0,0], 'markersize', 10);
hold on; plot(sal(eau13), temp(eau13), '.', 'color', [0.8,0,0], 'markersize', 10);
hold on; plot(sal(eau123), temp(eau123), '.', 'color', [0.8,0,0], 'markersize', 10);


xlabel('Salinit� (g.kg^{-1})');
ylabel('Temp�rature (�C)');
##title(['Diagramme T-S de tous les transects']);
title(['Transect ',num2str(i)]);
##title(['Diagramme TS de tous les transects']);
xlim([37.7,38.9]);
ylim([13,16.5]);
grid on
###################################################################################


#############################################################################
##f = figure;
##plot(dist(LIW), -pres(LIW), '.k', 'markersize', 10); # LIW BLACK
##hold on; plot(dist(b), -pres(b), '.k', 'markersize', 10); # SAL>38.7 MAGENTA
##
##### BLEU ###
##hold on; plot(dist(eau1), -pres(eau1), '*b', 'markersize', 6);
##hold on; plot(dist(eau2), -pres(eau2), '.b', 'markersize', 10);
##hold on; plot(dist(eau21), -pres(eau21), '.b', 'markersize', 10);
##
##hold on; plot(dist(eau3), -pres(eau3), '.g', 'markersize', 10);
##
##hold on; plot(dist(eau4), -pres(eau4), '*', 'color', [0.8,0,0], 'markersize', 6);
##
##### ORANGE ###
##hold on; plot(dist(eau5), -pres(eau5), '.', 'color', [1,0.8,0], 'markersize', 10);
##hold on; plot(dist(eau6), -pres(eau6), '.', 'color', [1,0.8,0], 'markersize', 10);
##hold on; plot(dist(eau56), -pres(eau56), '.', 'color', [1,0.8,0], 'markersize', 10);
##hold on; plot(dist(eau565), -pres(eau565), '.', 'color', [1,0.8,0], 'markersize', 10);
##
##hold on; plot(dist(eau7), -pres(eau7), '*','color', [1,0.8,0], 'markersize', 6);
##hold on; plot(dist(eau8), -pres(eau8), '*','color', [1,0.8,0], 'markersize', 6);
##hold on; plot(dist(eau9), -pres(eau9), '.','color', [1,0.8,0], 'markersize', 10);
##
##
##### VERT FONCE ###
##hold on; plot(dist(eau11), -pres(eau11), '*', 'color', [0,0.5,0], 'markersize', 7);
##hold on; plot(dist(eau111), -pres(eau111), '*', 'color', [0,0.5,0], 'markersize', 7);
##%hold on; plot(dist(eau1111), -pres(eau1111), '*', 'color', [0,0.5,0], 'markersize', 7);
##hold on; plot(dist(eau111111), -pres(eau111111), '*', 'color', [0,0.5,0], 'markersize', 6);
##
##### BORDEAUX ###
##hold on; plot(dist(eau12), -pres(eau12), '.', 'color', [0.8,0,0], 'markersize', 10);
##hold on; plot(dist(eau13), -pres(eau13), '.', 'color', [0.8,0,0], 'markersize', 10);
##hold on; plot(dist(eau123), -pres(eau123), '.', 'color', [0.8,0,0], 'markersize', 10);

##### PLOT FLUO #####
##figure;
##pcolor(dist,-pres,fluo);
##shading interp;
##colorbar;
##colorbar('ylabel','Fluorescence mg.L-1');
##caxis([0 0.65]);
#####################

##xlabel('Distance (km)');
##ylabel('Pression (dbar)');
##title(['Transect ',num2str(i)]);
##%title(['PL6']);
##
####ylim([-250,0]);
##ylim([-8.2,-7.8]);
saveas(i,['Diagramme TS Transect ',num2str(i),'(3).png']);
##
####saveas(f,['Masses(1).png']);

end  
