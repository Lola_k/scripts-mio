# Scripts MIO

Ces scripts ont été écrits durant mon stage de L3 (1 mois) au MIO (Marseille).
Le but de ce stage était d'étudier les caractéristiques physiques des masses d'eau rencontrées durant la campagne FUMSECK en mer Ligure,
et de les lier aux données biologiques.

Ce travail a été fait sur Python, Matlab et R en utilisant des fichiers netcdf (.nc) issus du MVP.

Tous les scripts sont en libre service, bien qu'ils ne soient pas forcément optimisés.
